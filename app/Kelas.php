<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends model
{
  public $table = 'kelas';

  protected $fillable =[
    'id','nama_kelas','jurusan'
  ];
}
?>
